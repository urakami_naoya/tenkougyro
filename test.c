
// PIC16F877A Configuration Bit Settings

// 'C' source line config statements

// CONFIG
#pragma config FOSC = HS        // Oscillator Selection bits (HS oscillator)
#pragma config WDTE = OFF       // Watchdog Timer Enable bit (WDT disabled)
#pragma config PWRTE = OFF      // Power-up Timer Enable bit (PWRT disabled)
#pragma config BOREN = OFF      // Brown-out Reset Enable bit (BOR disabled)
#pragma config LVP = OFF        // Low-Voltage (Single-Supply) In-Circuit Serial Programming Enable bit (RB3 is digital I/O, HV on MCLR must be used for programming)
#pragma config CPD = OFF        // Data EEPROM Memory Code Protection bit (Data EEPROM code protection off)
#pragma config WRT = OFF        // Flash Program Memory Write Enable bits (Write protection off; all program memory may be written to by EECON control)
#pragma config CP = OFF         // Flash Program Memory Code Protection bit (Code protection off)

// #pragma config statements should precede project file includes.
// Use project enums instead of #define for ON and OFF.

#include <xc.h>
#include <stdlib.h>
#include <stdint.h> //To use uint_16, etc.
#include "decoder.h"
#include "TenkouGPIO.h"
#include "spi_pins.h"
#include "spi_mode_3.h"
#include "uart.h"
#include "gyro.h"

int test_angle_velocity(void) {
    
    float data_1[3];
    float data_2[3];
    float data_3[3];
    float data_4[3];
    int status;
    char* buf;
    
    unsigned char uartSpace[] = {" "}; // To separate consecutive readings.
    unsigned char GYRO1_MsgX [] = {"G1_X:"};
    unsigned char GYRO1_MsgY [] = {"G1_Y:"};
    unsigned char GYRO1_MsgZ [] = {"G1_Z:"};
    unsigned char GYRO2_MsgX [] = {"G2_X:"};
    unsigned char GYRO2_MsgY [] = {"G2_Y:"};
    unsigned char GYRO2_MsgZ [] = {"G2_Z:"};
    unsigned char GYRO3_MsgX [] = {"G3_X:"};
    unsigned char GYRO3_MsgY [] = {"G3_Y:"};
    unsigned char GYRO3_MsgZ [] = {"G3_Z:"};
    unsigned char GYRO1_MsgT [] = {"G1_T:"};
    unsigned char GYRO2_MsgT [] = {"G2_T:"};
    unsigned char GYRO3_MsgT [] = {"G3_T:"};
    unsigned char cr = 0x0D;

    uartInit(9600);
    
    spi_pins_initialization();
    decoder_initialize();
    
    TRISD1 = 0;//GYRO1_CS
    RD1 = 1;//INITIALIZE
    
//    SPI_SENSE_ADS_CS_MAGADC1_INIT = 0; // Initialize MAGADC1 as output:
//    SPI_SENSE_ADS_CS_MAGADC1 = 1; //SEt MAGADC1 CS to high.
    
    select_gyro_information_t init_gyro_1 , init_gyro_2 , init_gyro_3 , gyro_1[3] , gyro_2[3] , gyro_3[3];
        init_gyro_1.cs_pin = 1;//SELECT SLAVE PIN RD1
        init_gyro_1.cs_port = &PORTD;//SELECT SLAVE PORT
        init_gyro_1.q_outpin = 8; //Do not need decoder
        
        init_gyro_2.q_outpin = 5;//Q5

        init_gyro_3.q_outpin = 6;//Q6

    for(int time = 0 ; time < 3 ; time++) {
        gyro_1[time] = init_gyro_1;
        gyro_2[time] = init_gyro_2;
        gyro_3[time] = init_gyro_3;
        gyro_1[time].axis = time;
        gyro_2[time].axis = time;
        gyro_3[time].axis = time;
    }
    
    for(;;)
    {   
        for(int time = 0 ; time < 3 ; time++)
        {
            data_1[time]=axis_angle_velocity(&gyro_1[time]);   //Read data from gyro-1 x-y-z axis 
            __delay_us(1);
            //data_4[0]=temp_gyro(&gyro_1[0]);   //Read temperature from gyro-1 
            //__delay_us(1);
            data_2[time]=axis_angle_velocity(&gyro_2[time]);   //Read data from gyro-2 x-y-z axis
            __delay_us(1);
            //data_4[1]=temp_gyro(&gyro_2[0]);   //Read temperature from gyro-2 
            //__delay_us(1);
            data_3[time]=axis_angle_velocity(&gyro_3[time]);   //Read data from gyro-2 x-y-z axis
            __delay_us(1);
            //data_4[2]=temp_gyro(&gyro_3[0]);   //Read temperature from gyro-3 
            //__delay_us(1);
        }
         //Reading temperature of each gyro 
        data_4[0]=temp_gyro(&gyro_1[0]);   //Read temperature from gyro-1 
        __delay_us(1);
        data_4[1]=temp_gyro(&gyro_2[0]);   //Read temperature from gyro-2 
        __delay_us(1);
        data_4[2]=temp_gyro(&gyro_3[0]);   //Read temperature from gyro-3 
        __delay_us(1);
        
        //unsigned char GYRO2_Msg[3];
        //memcpy(GYRO2_Msg,GYRO2_MsgX,sizeof[GYRO2_MsgX])

                                // Send GYRO1 data
         // GYRO1 1 axis
        buf = ftoa(data_1[0], &status);
        uartWriteBytes(GYRO1_MsgX,5);
        uartWriteBytes(buf,8);
        uartWriteBytes(uartSpace,1);
        // GYRO1 Y axis
        buf = ftoa(data_1[1], &status);
        uartWriteBytes(GYRO1_MsgY,5);
        uartWriteBytes(buf,8);
        uartWriteBytes(uartSpace,1);
        // GYRO1 Z axis
        buf = ftoa(data_1[2], &status);
        uartWriteBytes(GYRO1_MsgZ,5);
        uartWriteBytes(buf,8);
        uartWriteBytes(uartSpace,1);
        //uartWrite(&cr);
        // GYRO1 temperature
        buf = ftoa(data_4[0], &status);
        uartWriteBytes(GYRO1_MsgT,5);
        uartWriteBytes(buf,8);
        uartWrite(&cr);
               
                                // Send GYRO2 data
         // GYRO2 X axis
        buf = ftoa(data_2[0], &status);
        uartWriteBytes(GYRO2_MsgX,5);
        uartWriteBytes(buf,8);
        uartWriteBytes(uartSpace,1);
        // GYRO2 Y axis
        buf = ftoa(data_2[1], &status);
        uartWriteBytes(GYRO2_MsgY,5);
        uartWriteBytes(buf,8);
        uartWriteBytes(uartSpace,1);
        // GYRO2 Z axis
        buf = ftoa(data_2[2], &status);
        uartWriteBytes(GYRO2_MsgZ,5);
        uartWriteBytes(buf,8);
        uartWriteBytes(uartSpace,1);
        //uartWrite(&cr);
        // GYRO2 temperature
        buf = ftoa(data_4[1], &status);
        uartWriteBytes(GYRO2_MsgT,5);
        uartWriteBytes(buf,8);
        uartWrite(&cr);
        
                                // Send GYRO3 data
         // GYRO3 X axis
        buf = ftoa(data_3[0], &status);
        uartWriteBytes(GYRO3_MsgX,5);
        uartWriteBytes(buf,8);
        uartWriteBytes(uartSpace,1);
        // GYRO3 Y axis
        buf = ftoa(data_3[1], &status);
        uartWriteBytes(GYRO3_MsgY,5);
        uartWriteBytes(buf,8);
        uartWriteBytes(uartSpace,1);
        // GYRO3 Z axis
        buf = ftoa(data_3[2], &status);
        uartWriteBytes(GYRO3_MsgZ,5);
        uartWriteBytes(buf,8);
        uartWriteBytes(uartSpace,1);
        //uartWrite(&cr);
        // GYRO3 temperature
        buf = ftoa(data_4[2], &status);
        uartWriteBytes(GYRO3_MsgT,5);
        uartWriteBytes(buf,8);
        uartWrite(&cr);
    
    __delay_ms(500);
    }
}

int main (void) {
    int a = test_angle_velocity();
}


                       
