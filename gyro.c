#include "gyro.h"

/**
 * read_bit
 * 
 * Read MSB and LSB data from each axis
 * Using it in axis_angle_velocity function.
 * 
 */
static void read_bit(select_gyro_information_t* gyro_condition , uint8_t register_address , uint8_t* data){
    gyro_condition->q_outpin & 0x08 ? 
        select_slave(gyro_condition->cs_port, gyro_condition->cs_pin, 1) : decoder_select_slave(gyro_condition->q_outpin , 1);
    spiWrite_mode3(&register_address,1);   
    _delay(10);
    spiRead_mode3(data,1);
    gyro_condition->q_outpin & 0x08 ? setPinHigh(gyro_condition->cs_port, gyro_condition->cs_pin) : decoder_select_pin(7);
}

/**
 * axis_angle_velocity
 * 
 * Read converted angle velocity data from each Gyros axis.
 * 
 * Note
 * ---------
 * The structure is defined in header of TenkouGYRO
 * 
 * Usage example: Define in any order
 * ---------------------------------------------------------
 * select_gyro_information_t <NAME>;
 *      <NAME>.q_out = 0 ~ 7 , 8
 *      <NAME>.cs_port = &PORT<X>
 *      <NAME>.cs_pin = 0 ~ 7
 *      <NAME>.axis = 0 , 1 , 2
 * ---------------------------------------------------------
 * <X>: A ~ E
 * <NAME>: Freely name the structure
 * 
 * The detail:
 * <NAME>.cs_port and <NAME>.cs_pin: Select the slave select pin which is connected on devise.
 * 
 * <NAME>.q_outpin: Select CS pin through decoder.
 * 
 * <NAME>.axis: Select axis as follow.
 *      <NAME>.axis = 0: X
 *      <NAME>.axis = 1: Y
 *      <NAME>.axis = 2: Z
 */
float axis_angle_velocity(select_gyro_information_t* gyro_condition) {
    uint8_t config_reg[2] = {0x20,0x0F};
    gyro_condition->q_outpin & 0x08 ? 
        select_slave(gyro_condition->cs_port, gyro_condition->cs_pin, 1) : decoder_select_slave(gyro_condition->q_outpin , 1);
    spiWrite_mode3(config_reg,2);//Send device identification register
    gyro_condition->q_outpin & 0x08 ? setPinHigh(gyro_condition->cs_port, gyro_condition->cs_pin) : decoder_select_pin(7);
    
    uint8_t lsb_add_axis , msb_add_axis;
    switch(gyro_condition->axis) {  //Decide register address each axis
        case 0:  //X_axis
            lsb_add_axis = 0xA8, msb_add_axis = 0xA9;
            break;
        case 1:  //Y_axis
            lsb_add_axis = 0xAA, msb_add_axis = 0xAB;
            break;
        case 2:  //Z_axis
            lsb_add_axis = 0xAC, msb_add_axis = 0xAD;
            break;
    }
    
    uint8_t lsb;
    read_bit(gyro_condition , lsb_add_axis , &lsb);
    uint8_t msb;
    read_bit(gyro_condition , msb_add_axis , &msb);
    
    float angle_velocity;
    if (msb & 0x80) {
        long buff = (~((msb << 8) + lsb) + 1);
        angle_velocity = -(buff * 0.00875);
    } else {
        long buff = ((msb << 8) + lsb);
        angle_velocity = buff * 0.00875;
    }
    
    return angle_velocity;
}

float temp_gyro(select_gyro_information_t* gyro_condition) {
    uint8_t config_reg[2] = {0x20,0x0F};
    gyro_condition->q_outpin & 0x08 ? 
        select_slave(gyro_condition->cs_port, gyro_condition->cs_pin, 1) : decoder_select_slave(gyro_condition->q_outpin , 1);
    spiWrite_mode3(config_reg,2);//Send device identification register
    gyro_condition->q_outpin & 0x08 ? setPinHigh(gyro_condition->cs_port, gyro_condition->cs_pin) : decoder_select_pin(7);
    
    uint8_t out_temp = 0xA6;
    uint8_t Out_Temp;
    read_bit(gyro_condition , out_temp , &Out_Temp);
  
    float temp;
    if (Out_Temp & 0x80) {
        long buff = (~(Out_Temp) + 1);
        temp = -(buff);
    } else {
        long buff = (Out_Temp);
        temp = buff;
    }
    
    return temp;
}
