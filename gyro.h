/* * File:   gyro.h
 * Author:  Rigoberto and Urakami
 *
 * Created on April 11 Urakami Naoya 
 */

#ifndef GYRO_H
#define	GYRO_H

#include "spi_pins.h"
#include "spi_mode_3.h"
#include "decoder.h"

typedef struct select_gyro_information {
    uint8_t q_outpin;   //Select CS pin through decoder
    volatile unsigned char *cs_port;    //Slect CS port directly connecting pic to Gyro
    unsigned char cs_pin;   //Slect CS port directly connecting pic to Gyro
    uint8_t axis;   //Select Gyros axis[x , y ,z]
} select_gyro_information_t;

/**
 * Functions needed to make axis_angle_velocity
 * */
static void read_bit(select_gyro_information_t* gyro_condition , unsigned char register_address , unsigned char* data);

/**
 * Functions which it is available for users to use in main_sentence.
 * */
float axis_angle_velocity(select_gyro_information_t* gyro_condition);
float temp_gyro(select_gyro_information_t* gyro_condition);

#endif	/* GYRO_H */
